
Object Relational Mapping (ORM)
--------------------------------------------------------------------------------
ORM fornisce, mediante un'interfaccia orientata agli oggetti, tutti i servizi
inerenti alla persistenza dei dati, astraendo nel contempo le caratteristiche
implementative dello specifico RDBMS utilizzato.

Mediante l'ORM siamo quindi in grado di eseguire le canoniche operazioni
di creazione, lettura, aggiornamento e cancellazione dei dati (CRUD).
I programmatori possono utilizzare il loro linguaggio preferito per poter
interagire con il database senza dover manipolare il DDL

Ad esempio

.. code-block:: sql

    SELECT * FROM users WHERE id=1

diventa

.. code-block:: python

    user = Users.objects.get(id=1)

.. todo:: descrivere meglio ORM

.. todo:: aggiungere alcune query di esempio dall'app polls