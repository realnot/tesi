
Definire e gestire i requisiti
--------------------------------------------------------------------------------

Ogni progetto normalmente richiede che siano soddisfatti determinati requisiti.
Ad esempio, se intendiamo utilizzare Django come framework Python di backend
dobbiamo aggiungerlo alla lista dei requisiti

.. code-block:: bash

    echo "django" > requirements.txt

Quindi installare i requisiti nell'ambiente virtuale creato in precedenza

.. code-block:: bash

    pip install -r requirements.txt

Esistono altri metodi per la gestione dei requisiti nei progetti Python/Django,
ad esempio **Jeff Triplett** ha proposto di creare una cartella `requirements`,
con al suo interno quattro file di testo come `base.txt`, `local.txt`,
`staging.txt`, `production.txt` o qualsiasi altro file di testo che meglio
rappresenti le impostazioni di configurazione del progetto.

::

    requirements
    ├── base.txt
    ├── local.txt
    ├── production.txt
    └── staging.txt

Possiamo inserire in `base.txt` le dipendenze usate in tutti gli ambienti,
mentre in `local.txt` solo quelli utilizzate durante lo sviluppo e in
`production.txt` quelle per la messa in opera del progetto.

Esistono altre possibilità per gestire le dipendenze che non fanno uso di
file di testo. Provate a pensare nel caso in cui installiamo un pacchetto
che ha diverse dipendenze, se in futuro decidessimo di rimuoverlo, tutte le
dipendenze che il paccheto ha installato rimarrebbero nell'ambiente virtuale.

A questo proposito il sistema di packaging di python - setup.py [#ockd]_ offre
un ottima soluzione per gestire e tenere traccia delle dipendenze del progetto.

.. code-block:: python

    from setuptools import setup

    setup(name='tesi',
          version='0.1',
          description='The best tesi in the world',
          url='http://bitucket.com/realnot',
          author='Mauro Crociara',
          author_email='mauro.crociara@gmail.com',
          license='MIT',
          install_requires=[
              'django',
              'psycopg2',
          ],
          zip_safe=False)

È possibile installare le dipendenze mediante l'esecuzione del comando

.. code-block:: bash

    pip install .

.. [#ockd] Python packaging - gestione delle dipendense - https://python-packaging.readthedocs.io/en/latest/dependencies.html
