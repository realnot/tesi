
Creazione dell'ambiente di sviluppo
--------------------------------------------------------------------------------

Prima di poter iniziare il progetto occorre preparare un ambiente di sviluppo
dedicato ed isolato, in modo che i requisiti siano indipendenti l'uni dagli
altri.

Per fare fare questo esistono vari strumenti, tra cui virtualenv.

Virtualenv crea un ambiente contenente le cartelle in cui poi verranno
installati i relativi pacchetti o dipendenze del progetto, ma non solo,
permette di specificare quale versione dell'interprete Python utilizzare senza
condividere le librerie con gli altri ambienti.

.. code-block:: bash

    pip install virtualenv
    virtualenv polls

Questo crea una ambiente virtuale di nome polls con l'ambiente Python. Tutte le
librerie Python che installeremo d'ora in poi verranno create in:

.. code-block:: bash

    source ~/Workspace/.envs/polls/lib/python3.4/site-packages

Per poter entrare nell'ambiente appena creato, occore eseguire lo script
`activate` come segue.

.. code-block:: bash

    source ~/Workspace/.envs/polls/bin/activate

La configurazione di virtualenv permette di specificare, tra le altre cose,
la cartella in cui tutti gli ambienti virtuali verrano installati. In questo
esempio ho specificato che tutti gli ambienti virtuali vengano installati in

.. code-block:: bash

    ~/Workspace/.envs

**Doug Hellmann** ha realizzato una valida estensione di questo strumento di
nome Virtualenvwrapper [#oxyq]_.

Come suggerisce il nome, virtualenvwrapper è un wrapper per gestire gli
ambienti virtuali creati mediante virtualenv. Avremmo potuto ottenere lo stesso
risultato sopra mediante

.. code-block:: bash

    mkvirtualenv polls

che crea e attiva automaticamente un ambiente virtuale di nome polls. È
possibile disabilitare l'ambiente virtuale mediante il comando `deactivate`.

.. [#oxyq] Il progetto virtualenvwrapper è disponibile all'indirizzo https://virtualenvwrapper.readthedocs.io/en/latest/
