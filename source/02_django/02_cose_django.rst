
Cos'è Django?
--------------------------------------------------------------------------------

Django è un framework Python per la creazione di applicazioni web che segue
il pattern architetturale MVT (Model View Teample) [#dfhR]_. L'obbiettivo principale
è rendere semplice la progettazione e lo sviluppo di applicazioni web complesse.
Django è stato progettato per aiutare gli sviluppatori a portare le applicazioni
dall'idea alla realizzazione il più rapidamente possibile, grazie agli strumenti
già implementati e pronti all'uso come:

:Templating: un sistema di templating che utilizza il concetto di ereditarietà
    derivante dalla programmazione orientata agli oggetti (OOP);

:Internazionalizzazione: un sistemata in grado di tradurre i componenti Django
    in una moltitudine di lingue;

:Middleware: supporto ai middleware, compresi quelli personalizzati, in grado
    di intervernire nei vari steps del ciclo request/response;

:Autenticazione: un sistema per la registrazione e la gestione degli utenti;

:Serializzazione: un sistema un grado di produrre dati in formato XML e/o JSON
    dai modelli;

:Unit-Test: un framework per il testing;

:Server: un server per lo sviluppo rapido;


Nell'autunno del 2003 **Adrian Holovaty** e **Simon Willison** abbandonarono PHP e
iniziarono a usare Python per sviluppare i loro siti web. Mentre costruivano siti
intensivi e riccamente interattivi come Lawrence.com, iniziarono a estrarre un
framework di sviluppo Web generico, che consentiva loro di creare applicazioni Web
sempre più rapidamente. Hanno ottimizzato costantemente questa struttura,
aggiungendo miglioramenti nell'arco di due anni. [#dfhR]_

Django è stato sviluppato perché gli autori, dopo aver studiato e provato diversi
framework web, non erano soddisfatti della situazione attuale, quindi influenzati
dallo stato delle cose, hanno preso molte buone idee e scritto il framework.

.. [#dfhR] Perché esiste il progetto Django - https://docs.djangoproject.com/en/dev/faq/general/#why-does-this-project-exist

Come funziona Django?
--------------------------------------------------------------------------------

Vediamo il ciclo di vita di una richiesta HTTP in questo schema che mostra la
request / response

.. image:: ../_static/django_request_response_cycle.png
    :scale: 60 %
    :align: center

Quando digitiamo un URL nel browser e premiamo `enter` il browser (il `client`) mediante
il protocollo HTTP effettua una richiesta al `web server` (Nginx / Apache). Il server inoltra
la richiesta a Django che invoca il `middleware delle richieste`. Una volta ottenuta la request,
Django, mediante l'`URL resolution`, verifica se l'URL è presente tra gli URLs pattern definiti
a livello root e successivamente nelle singole applicazioni (urls.py). Se avviene il match,
ovvero se la risorsa nella request viene assocciata correttamente, Django contatta il
`middleware delle views` che a sua volta invoca la funzione associata all'URL. Arrivati a questo
punto esistono vari scenari che elencherò di seguito.

    1. La logica della view esegue un'operazione CRUD [#wKDc]_ verso il database di backend per poi
       ritornare una HTTP response che verrà inoltrata al middleware delle responses.
       Il middleware gestisce la response inviandola al server, il quale provvederà a
       comunicare la risposta al client.

    2. La view non necessita di contattare alcun database di backend e quindi ritorna
       immediatamante una response (ad esempio quando vogliamo mostrare un messaggio
       all'utente)

L'ausilio del template (e quindi del template middleware) è del tutto opzionale ed
è sempre meno utilizzato, poiché le moderne applicazioni fanno uso di framework di
frontend per mostrare i dati. Alcuni esempi sono AngularJS [#cibu]_, ReactJS [#PBir]_, VueJS [#JXry]_,
giusto per citarne alcuni. In questo contesto, la view ritornerà dati serializzati
in formato JSON [#xqgm]_ oppure XML [#UQgN]_ che verranno successivamente elaborati dal client (dall'applicazione
del client).

Se la view genera un eccezione, perché ad esempio non trova il record richiesto dall'utente
nel database, oppure perché il client genera un eccessivo numero di richieste questa verrà
gestita dal middleware delle eccezioni che a sua volta contatterà il middleware delle
response che notificare al client l'errore.

Django offre la possibilità mediante il file di configurazione *settings.py* di configurare
i middleware e di aggiungerne di nuovi.

.. code-block:: python

    MIDDLEWARE = [
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ]

**MIDDLEWARE** è una lista python contenente tutti i middleware che verrano eseguiti, in
ordine, dall'alto verso il basso. Dalla lista si evidenzia ad esempio il middleware
incaricato di gestire l'autenticazione e il middleware per la gestione della sessione,
giusto per citarni alcuni.

.. image:: ../_static/middleware_stack.png
    :scale: 60 %
    :align: center


La possibilità di intervenire in qualsiasi punto request/response cycle è senza dubbio
uno dei punti di forza di Django. Un esempio di utilizo di un middleware customizzato
potrebbe essere quello di condividere alcune risorse prese dal database prese dal database
per metterle a disposizione in tutte le view (al costo di una query al database). Un altro
caso d'uso potrebbe essere quello di scrivere un middleware che agisca come sistema di logging.
Le possibilità sono veramente molte.

Nel spiegare in dettaglio il request/response cycle, ho volutamente tralasciato uWSGI [#SuJl]_.

Un web server tradizionale come Nginx / Apache non sa in alcun modo come eseguire
applicazioni Python, pertanto la sua presenza è vitale nell'eseguire applicazioni Django.

Tuttavia uWSGI è giovane e non ottimizzato per gestire grosse quantità di traffico, per
questo motivo, nelle moderne applicazioni web che necessita di scalare viene aggiunto
al layer soprastante un server Nginx (o simile) in grando di effettuare
load balacing, gestire le risorse statiche in modo efficiente, effettuare caching dei dati
in modo più intelligente, senza considerare la maggior sicurezza.


.. [#wKDc] CRUD - https://en.wikipedia.org/wiki/Create,_read,_update_and_delete
.. [#PBir] ReactJS - https://reactjs.org/
.. [#cibu] AngularJS - https://angularjs.org/
.. [#JXry] VueJS - https://vuejs.org/
.. [#xqgm] JSON - https://en.wikipedia.org/wiki/JavaScript_Object_Notation
.. [#UQgN] XML - https://en.wikipedia.org/wiki/XML
.. [#SuJl] uWSGI - https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface