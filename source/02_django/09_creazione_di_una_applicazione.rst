
Creazione di un'applicazione
--------------------------------------------------------------------------------

Proseguiamo creando un Blog come esempio di applicazione. Ogni applicazione di
Django consiste in un pacchetto Python che segue alcune convenzioni. Come per
la creazione del progetto, Django mette a disposizione un comando da eseguire
all'interno del progetto per creare applicazioni in modo semplice

.. code-block:: bash

    django-admin startapp blog

Questo genera il seguente albero di files e cartelle

::

    blog
    ├── admin.py
    ├── apps.py
    ├── __init__.py
    ├── migrations
    │   └── __init__.py
    ├── models.py
    ├── tests.py
    └── views.py

Che hanno il seguente scopo:

:admin.py: registrazione dei modelli per renderli accessibili attraverso il sito di
           amministrazione django

:apps.py: la configurazione dell'applicazione

:migrations: le migrazioni del database

:models.py: lo schema del database

:tests.py: i tests dell'applicazione

:views.py: la logica dell'applicazione

Una volta creata l'applicazione dobbiamo registrarla nella lista delle
applicazioni python all'interno del file settings.py generato durante la
creazione del progetto.

.. code-block:: python

    # Somewhere inside mysite/settings.py

    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',

        # Project applications
        'blog.apps.BlogConfig',
    ]

