
MVT (Model View Template) pattern
--------------------------------------------------------------------------------
Chi si avvicina a Django per la prima volta, dal punto di vista architetturale,
associa il framework al pattern MVC (Model View Controller) il cui scopo è la
concern separation tra la business logic e la presentation logic.

In Django, la `V` di MVC è la view, che descrive il modo e il tipo di
informazione che deve essere presentata all'utente. Il contenuto, non lo stile,
che invece viene gestito dal template.

In Django l'intero framework viene considerato il controller, ovvero colui
che invia le richieste HTTP alla `View` corretta, passando per l'URL dispatcher.

**Luke Plant** core developer di Django e brillante application developer mette
in luce più della semplice distizione semantica che si interpone tra i due
pattern.

HTTP è un protocollo senza stato (stateless), il client ed il server non
mantengono alcuna informazione circa lo stato. MVC ruota intorna alla gestione
dello stato. Il model, la view ed il controller sincronizzano il dato
in modo che sia integro in ogni layer archietturale.

Django non segue il pattern MVC [#ivyu]_ [#TZwx]_

.. [#ivyu] MVC is not a helpful analogy for Django - Luke Plant - https://lukeplant.me.uk/blog/posts/mvc-is-not-a-helpful-analogy-for-django/
.. [#TZwx] Django appears to be a MVC framework, but you call the Controller the “view”, and the View the “template”. How come you don’t use the standard names? https://docs.djangoproject.com/en/dev/faq/general/#django-appears-to-be-a-mvc-framework-but-you-call-the-controller-the-view-and-the-view-the-template-how-come-you-don-t-use-the-standard-names