
Server di sviluppo
--------------------------------------------------------------------------------

Per accellerare e semplificare lo sviluppo, Django mette a dispozione un server
di sviluppo scritto in Python, evitando così la configurazione di un server di
produzione come Apache o Nginx.

Per avviare il server, è sufficiente eseguire:

.. code-block:: bash

    python manage.py runserver

Che avvia il server all'indirizzo 127.0.0.0 sulla porta 8000. È possibile
passare l'indirizzo e la porta come parametri se necessario, ad esempio per
permettere ai propri colleghi di raggiungere il server durante la fase di
sviluppo

.. code-block:: bash

    python manage.py runserver 0:8080

Un altro aspetto interessante è che il server verifica se vi sono stati dei
cambiamenti sul codice e si ricarica in automatico evitando così il riavvio
manuale mediante l'esecuzione dell'apposito comando.