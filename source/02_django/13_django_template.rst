
Django: Template
--------------------------------------------------------------------------------

Nella sezione precedente, parlando delle viste, abbiamo spiegato come django
mediante la funzione `render` prende in ingresso tre parametri

    1. La request;
    2. Il template;
    3. Un contesto;

.. todo:: introdurre meglio l'argomento

senza però parlare dei templates. Django essendo un web framework, necessita di
un sistema per poter generare templates in modo dinamico. Un template contiene gli
elementi statici HTML, ma anche CSS e Javascript.

Django definisce un API standard per il caricamento ed il rendering dei templates.
Il caricamento consiste nel trovare il template mediante il template engine e
caricarlo in memoria, mentre il rendering consiste nella trasformazione del template
in una stringa, regolarmente formattata prima di essere ritornata. [#rPbi]_

La creazione dei template avviene mediante il DTL (Django Template Language) che grazie
a tag specifici e variabili che verranno sostituite durante il parsing del template
permette di generare un contenuto dinamico.

Creaiamo un albero di cartelle e file all'interno dell'applicazione blog che rispecchi
il seguente:


::

    blog
    └── templates
        └── blog
            └── index.html


Il motivo per cui creiamo un'altra cartella con lo stesso nome dell'applicazione come
sottodirectory di templates è che essa agisce da namespace. Il template engine non
è in grado di distinguere due files che hanno lo stesso nome, ad esempio due
file `index.html` figli della cartella templates di due applicazioni diverse

::

    blog
    └── templates
        └── index.html
    foo
    └── templates
        └── index.html

Ora supponiamo che in una qualche view sia dichiarata la funzione render come segue:


.. code-block:: python

    return render(request, 'index.html', context)


Il template engine finirebbe per caricare la prima occorrenza trovata che produrrà
un errore nella maggiorparte dei casi. L'utilizzo dei namespace impedisce ogni
ambiguità


.. code-block:: python

    return render(request, 'blog/index.html', context)


Ovvero, carica il template index.html dell'applicazione Blog.


Dentro al file index.html mediante il DTL scriviamo il seguente template:


.. code-block:: html+django

    {% if posts %}
        <ul>
        {% for post in posts %}
            <li><a href="{% url 'post_detail' post.slug %}">{{ post.title }}</a></li>
        {% endfor %}
        </ul>
    {% else %}
        <p>Il blog non contiene post</p>
    {% endif %}


Il nostro template mostrerà la lista dei post nel caso in cui ve ne fosse almeno uno,
altrimento la scritta "il blog non contiene post".

DTL (Django Template Language)
--------------------------------------------------------------------------------

Come accennato in precedenza, un template non è altro che una stringa renderizzata
dal template engine. Vediamo ora alcuni strumenti che rendono il DTL uno strumento
semplice e potente.

Le variabili
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le variabili permettono di estrapolare le informazioni da un contesto passato come
argomento della funzione render nelle viste. Una varibile viene identificata da
una coppia di parentesi graffe.

.. code-block:: html+django

    {{ post }}

Per poter accedere agli attributi (nel caso in cui post fosse un oggetto),
useremo la notazione puntata

.. code-block:: html+django

    {{ post.author }}

Quando il template engine incontra una variabile, quest'ultima viene valutata
e rimpiazzata con il risultato corretto. Nel caso in cui la variabile non fosse
trovata, il template engine usare il valore dell'opzione `string_if_invalid`
che di default è impostato a stringa vuota `' '`.

I filtri
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

I filtri permettono di modificare il valore delle varibili e spesso vengono usati
per scopi rappresentativi più che logici. Per poter utilizzare un filtro è sufficiente
postporre il carette barra verticale (o pipe) alla variabile, seguita dal nome del filtro.
Alcuni esempi dell'utilizzo dei filtri sono:

.. code-block:: html+django

    {{ name|lower }}

Per trasformare in minuscolo il valore di una variabile

.. code-block:: html+django

    {{ value|default:"nothing" }}

Per impostare un valore di default nel caso in cui la variabile si null

.. code-block:: html+django

    {{ birthday|date:"M d, Y" }}

Per modificare il formato di rappresentazione di una data

I Tag
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

I tag sono una caratteristica molto importante del template language, essi
permettono di controllare il flusso e la logica di presentazione all'interno
del template. Il loro utilizzo avviene mediante una parentesi graffa seguita
dal carattere percentuale:

.. code-block:: html+django

    {% my_tag %}

Alguni template tag richiedono un tag di chiusura, come abbiamo mostrato nel
template index.html. Ad esempio l'istruzione condizionale if viene scritta nel
seguente modo:

.. code-block:: html+django

    {% if condition %}
        do something ...
    {% else %}
        do something else ...
    {% endif %}

Lo stesso vale per i cicli. Supponiamo di voler stampare la lista degli hashtag
di tutti i post:

.. code-block:: html+django

    {% if posts %}
        {% for post in posts %}
            <strong>tag list:
            {% for tag in post.tags %} tag {% endfor %}
            </strong>
        {% endfor %}
    {% else %}
        <p>There aren't posts</p>
    {% endif %}


L'ereditarietà
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Una delle parti più potenti e complesse del template engine è l'ereditarietà
dei template. L'ereditarietà ci permette di creare un modello base, che solitamente
contiene le parti comuni di tutte le pagine e altri blocchi che poi verrano sovrascritti
dai modelli figli. Il modo più semplice per spiegarlo è attraverso un esempio.

Il layout di un blog potrebbe essere qualcosa simile al seguente

.. image:: ../_static/blog_layout.png
    :scale: 60 %
    :align: center

La parti che tutte le pagine del blog avranno in comune saranno `l'header` ed il `footer`.
La sezione `aside` può essere utilizzata per mostrare le meta informazioni dell'articolo
che stiamo visualizzando, ma potrebbe anche essere sostituita da una `navbar` ad esempio.
La libertà di scelta in questo contesto è molto ampia, dipende dalle esigenze e specifiche di
progetto.

Definiamo il layout mostrato sopra mediante il DTL, creando un file di nome `base.html`
con il seguente contenuto

.. code-block:: html+django

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <link rel="stylesheet" href="style.css" />
        <title>{% block title %}My amazing blog{% endblock %}</title>
    </head>
    <body>
        <header>
            {% block header %}<p>The header content goes here...</p>{% endblock %}
        </header>
        <div id="content">
            {% block content %}<p>This block must be inherited</p>{% endblock %}
        </div>
        <aside>
            {% block sidebar %}
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/contacts/">Contacts</a></li>
            </ul>
            {% endblock %}
        </aside>
        <footer>
            {% block footer %}<p>Copyright stuff goes here...</p>{% endblock %}
        </footer>
    </body>
    </html>

Se diamo uno sguardo più attento al template notiamo che esso contiene cinque template tag:

    1. block title
    2. block header
    3. block content
    4. block sidebar
    5. block footer

Il template tag `block` dice al template engine che questa porzione di codice può essere
sovrascritta dal modello figlio che eredita il seguente template. Tutti i template figli
che erediteranno dal template `base.html` potranno sovrascrivere la relativa sezione.

Modifichiamo ora il template creato in precedenza `post_detail.html`, ovvero il template
designato per mostrare il dettaglio di un post, facendo in modo che erediti da `base.html`
sovrascrivendo solamente le parti necessarie.


.. code-block:: html+django

    {% extends "blog/base.html" %}

    {% block title %} <h1>{{ post.title }}</h1> {% endblock%}

    {% block content %}
        <span>Data: {{ post.posted }}</span>
        <div>{{ post.body }}</div>
    {% endblock %}

In questo snippet, abbiamo usato il template tag `extends` per estendere il template `base.html`
mostrato in precedenza e successivamente sovrascritto il blocco `title` ed il blocco `content`
per mostrare il titolo ed il contenuto (dinamico) per ogni post visualizzato. Se volessimo
apportare delle modifiche in tutti i post, basterà agire nel template figlio, mentre se volessimo
cambiare il layout¸basterà agire nel template base per fare in modo che le modifiche si propaghino
in tutti i template figli. L'ereditarietà applicata ai template permette il riutilizzo del codice
e di creare componenti che potranno essere applicate in qualsiasi punto dell'ereditarietà.

Qui ho voluto mostrare solo alcune delle funzionalità più importati del Django Template Language.
Il DTL non si limita alle variabili, filtri e template tags, ma offre molto di più. La documentazione
ufficiale è un ottimo punto di partenza.


.. [#rPbi] Django Templates - https://docs.djangoproject.com/en/dev/topics/templates/
.. [#kvTP] DTL (Django Template Language) - https://docs.djangoproject.com/en/dev/ref/templates/language/

