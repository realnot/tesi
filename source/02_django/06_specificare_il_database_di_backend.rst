
Specificare il database di backend
--------------------------------------------------------------------------------

Django di default specifica SQLite come database di backend che è incluso in
Python. Probabilmente questa è la scelta migliore perché permette di provare
il framework senza dover installare ulteriori requisiti e senza preoccuparsi
nel mettere le mani nel file *settings.py*.

Di seguito la configurazione di default per SQLite

.. code-block:: python

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }

la variabile `DATABASES` non è altro che un dizionario python. Il dizionario
contiene una chiave di nome `defaults` la quale ha come valore un altro
dizionario contenente la configurazione del database

:ENGINE: specifica il database di backend da utilizzare.

:NAME: specifica il nome del database. Nel caso di SQLite, sarà il nome
    del file sul computer preceduto dal path assoluto per raggiungerlo.

Altri valori per la chiave *ENGINE* sono:

* (PostgreSQL) - django.db.backends.postgresql
* (MySQL) - django.db.backends.mysql
* (Oracle) - django.db.backends.oracle

In aggiunta ai database supportati ufficialmente, sono disponibili alcuni database
di terze parti come **IBM DB2**, **Microsoft SQL server**, **ODBC**, giusto per
citarne alcuni. [#dlce]_

La chiave *default* del dizionario *DATABASES* è un alias e per Django ha un
ruolo chiave, infatti Django utilizza il database di default quando nessun
altro database è specificato.

Il dizionario *DATABASES* può contenere molteplici aliases e quindi mappare
diversi databases in base alle esigenze del progetto.

.. code-block:: python

    DATABASES = {
        'default': {
            'NAME': 'blog-db',
            'ENGINE': 'django.db.backends.postgresql',
            'USER': 'postgres_user',
            'PASSWORD': 'foo123'
        },
        'auth': {
            'NAME': 'auth-db',
            'ENGINE': 'django.db.backends.mysql',
            'USER': 'mysql_user',
            'PASSWORD': 'spam123'
        }
    }

Nel caso di un database diverso da SQLite possono essere specificati altri
parametri come

:USER: L'utente del database

:PASSWORD: La password per accedere alla base di dati

:HOST: L'indirizzo per raggiungere il database

:PORT: La porta del database

.. [#dlce] Using a 3rd-party database backend - https://docs.djangoproject.com/en/dev/ref/databases/#using-a-3rd-party-database-backend