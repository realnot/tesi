
Django Framework
=====================================================================================

.. toctree::
   :maxdepth: 3
   :caption: Indice:

   02_cose_django
   03_creazione_dell_ambiente_di_sviluppo
   04_definire_e_gestire_i_requisiti
   05_creare_un_progetto
   06_specificare_il_database_di_backend
   07_object_relational_mapping
   08_server_di_sviluppo
   09_creazione_di_una_applicazione
   10_mvt_pattern
   11_django_model
   12_django_view
   13_django_template