
Django: Views
--------------------------------------------------------------------------------

In Django le views non sono altro che delle funzioni che prendono una richiesta
HTTP in ingresso e ritornano una risposta. La risposta può essere di qualsiasi
tipo, come il contenuto HTML di una pagina web, un reindirizzamento, un'immagine
o dati serializzati.

Editiamo il file views.py dell'applicazione blog e scriviamo una semplice view

.. code-block:: python

    from django.http import HttpResponse

    def index(request):
        return HttpResponse("What's up man?? I'm your response!")

Per fare in modo che questa view sia raggiungibile, dobbiamo associarla ad URL,
quindi creaiamo un file urls.py (sempre all'interno della nostra applicazione)
e scriviamo:

.. code-block:: python

    from django.urls import path

    from . import views

    urlpatterns = [
        path('', views.index, name='index'),
    ]

urlpatterns è una lista contenente tutti i path della nostra applicazione.
Il path non è altro che una funzione che prende in ingresso tre parametri:

    - L'URL dell'applicazione
    - La funzione (vista) che verra chiamata
    - Il nome dell'URL

.. todo:: descrivere meglio questo paragrafo

Il nome dell'URL è molto importante perché permette il disaccoppiamento tra
l'URL ed il riferimento alla stessa. Supponiamo di scrivere un template, il
quale al suo interno riporta il link ad una risorsa della nostra applicazione,
cosa succederebbe se in futuro decidessimo che quell'URL non dovrà più puntare
ad una determinata funzione? Due soluzioni:

    1. la prima è quella di modificare il path all'interno di urls.py
       e in tutti i template che fanno uso di quel path. Ovviamente molto
       sconveniente, specialmente nei progetti di grosse dimensioni.

    2. La seconda, molto più efficace, è quella di dare un nome al path per poi
       cambiare l'URL o la vista in caso di necessita. I puristi del DRY
       (Don't Repeat Yourself) troveranno la seconda soluzione molto più sensata.

Esempio di un hardcoded, tightly-coupled URL:

.. code-block:: html

    <li><a href="/blog/{{ post.slug }}/">{{ post.title }}</a></li>

Esempio di un URL non hardcoded:

.. code-block:: html

    <li><a href="{% url 'post_detail' post.slug %}">{{ post.title }}</a></li>

Il secondo esempio fa uso del nome 'post_detail' associato all'URL per poterla
tradurre correttamente. Se in futuro la nostra applicazione dovesse cambiare
nome, ad esempio da *Blog* a *Magazine*, non dovremmo mettere le mani in tutti
i template di Django.

Nonostante il file urls.py dell'appliazione blog non riusciamo a
raggiungere la nostra vista, perchè Django non è ancora in grado di
di trovare il file urls.py definito a livello di applicazione.

Il prossimo passo è quello di creare un file radice urls.py a livello di progetto
che punti ai files urls.py delle varie applicazioni, nel nostro caso blog/urls.py

Facciamo un po di chiarezza:

::

    mysite
    ├── manage.py
    ├── mysite
    │   ├── __init__.py
    │   ├── settings.py
    │   ├── urls.py
    │   └── wsgi.py
    └── blog
        ├── __init__.py
        ├── admin.py
        ├── apps.py
        ├── migrations
        ├── models.py
        ├── tests.py
        ├── urls.py
        └── views.py

urls.py all'interno di mysite è il file contenente le URLs di progetto. La
lista urlpatterns al suo interno generalmente include gli URLs patterns
(includendo i files urls.py) delle singole applicazioni, come avviene nel
nostro caso

A questo punto è possibile contattare la vista creata sopra semplicemente
aprendo il browser in http://localhost:8000/blog/

Che cosa sta succedendo? Il client invia una richiesta HTTP al server, che la
inoltra a WSGI il quale chiama il middleware delle richieste. Il middleware
effettua un URLs lookup/resolution all'interno del root urls.py, il quale
trova un associazione in `blog`, quindi include il file urls.py di blog e
trova la seconda associazione. Una volta avvenuto il match (sì è una regex)
viene chiamata la vista associata.

Scriviamo una vista in views.py che mostra cinque articoli presenti nel
database

.. code-block:: python

    from django.shortcuts import render
    from django.shortcuts import get_object_or_404

    from .models import Post, Category

    def index(request):
        posts = Post.objects.all()[:5]
        return render(request, 'blog/index.html', {'posts': posts})

Abbiamo creato una vista di nome *index* che prende in ingresso come
parametro una richiesta http. La vista dichiara una variabile di nome posts
la quale contiene cinque istanze di tipo Post all'interno di un oggetto di
tipo QuerySet.

Ora è il turno della chiamata alla funzione **render** che prende in ingresso
tre parametri:

    1. Un oggetto di tipo request
    2. Un template
    3. Un contesto (l'istanza di Question)

Il contesto è un dizionario e può contenere qualsiasi cosa. Questo in Django
è ciò che viene chiamato **shortcut**, proprio come la funzione `get_object_or_404`.
Esistono diversi shortcut in Django per semplificare il lavoro e visto che
prendere un template, iniettare un contesto e ritornare una HttpResponse è
molto comune, Django ci viene in aiuto con la funzione `render`.


Ora scriviamo una vista che mostra il dettaglio di un Post.

.. code-block:: python

    def post_detail(request, slug):
        post = get_object_or_404(Post, slug=slug)
        return render(request, 'blog/post_detail.html', {'post': post})

Abbiamo creato una vista che prende in ingresso, oltre alla richiesta, lo slug [#UKrQ]_
di un post. Lo slug successivamente viene passato come argomento alla funzione
**get_object_or_404**, che, come suggerisce il nome, ritorna un instanza
dell'oggetto di tipo Post. Se la query al database fallisce perché
il record con lo slug passato come argomento non viene trovato, la funzione
lancia un'eccezione `DoesNotexist` che a sua volta lancia un Http404.

A questo punto, la variabile locale alla funzione può contenere un istanza
di Post, oppure un eccezione 404.

Giusto per completezza, avremmo potutto ottenere lo stesso risultato sopra
riscrivendo la vista come segue:

.. code-block:: python

    from django.http import Http404
    from django.shortcuts import render
    from polls.models import Post

    def detail(request, slug):
        try:
            post = Post.objects.get(slug=slug)
        except Post.DoesNotExist:
            raise Http404("Post does not exist")
        return render(request, 'blog/post_detail.html', {'post': post})

La vista `detail` però non è ancora raggiungibile, per poterla eseguire dobbiamo
configurare l'URL e mapparla con la funzione. Apriamo il file urls.py e scriviamo
questa associazione:

.. code-block:: python

    urlpatterns = [
        url(r'^$', views.index, name='index'),
        url(r'^(?P<slug>[\w-]+)/$', views.post_detail, name='post_detail'),
    ]

La funzione detail prende in ingresso oltre alla richiesta HTTP, lo slug di un
Post. Questo post viene passato nell'URL prima della chiamata. Se il `match`
avviene, allora la vista post_detail all'interno del modulo views viene invocata.
Il primo argomento della funzione **url** è una regex (Regular Expression)
ed il pattern definisce un argomento di nome `slug` di tipo `\w` metacarattere,
seguito dal carattere`-` Hyphen di lunghezza uno o superiore (+)

A questo punto la vista detail viene invocata correttamente se si
punta il browser all'indirizzo http://localhost:8000/blog/first-post/

.. [#UKrQ] Slug è un termine derivante dal settore dell'editoria, ma in questo contesto
    indica la parte finale di un URL resa human-readable per migliorare l'indicizzazione
    da parte dei motori di ricerca.
    - https://stackoverflow.com/questions/427102/what-is-a-slug-in-django
    - https://www.nytimes.com/times-insider/2014/11/24/whats-in-a-slug/