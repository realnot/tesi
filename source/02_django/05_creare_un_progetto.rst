
Creare un progetto
--------------------------------------------------------------------------------

Dopo aver eseguito il gestore dei pachetti di Python **pip** per installare
le dipendenze, possiamo creare un progetto Django.

Django mette a disposizione un comando per la creazione dei progetti per
generare la struttura iniziale.

.. code-block:: bash

    django-admin startproject mysite

Il risultato è una cartella di nome mysite (la cartella del progetto) con
al suo interno i seguenti files:

::

    mysite
    ├── manage.py
    └── mysite
        ├── __init__.py
        ├── settings.py
        ├── urls.py
        └── wsgi.py

I file generati hanno il seguento scopo:

:`manage.py`: uno script per interagire e gestire il progetto

:`mysite`: la cartella contenente la configurazione del progetto.

:`__init__.py`: un file vuoto che dice a Python di trattare la cartell come un modulo.

:`settings.py`: contiene le impostazioni di base del progetto.

:`urls.py`: contiene gli urls pattern di progetto.

:`wsgi.py`: contiene la configurazione per poter eseguire il progetto mediante WSGI.

Il file `settings.py` permette di impostare tra le altre cose il database di
backend del progetto che di default è SQLite, la lista dei middleware, la lista
dei backend di autenticazione, la lista delle applicazioni installate e molto
altro.
