
Django: Model
--------------------------------------------------------------------------------

Iniziamo lo sviluppo della nostra applicazione definendo lo schema della base
dati

.. code-block:: python

    # -*- coding: utf-8 -*-
    from django.db import models


    # Create your models here.
    class Category(models.Model):
        title = models.CharField(max_length=127, db_index=True)
        slug = models.SlugField(max_length=127, db_index=True)

        def get_absolute_url(self):
            return ('view_blog_category', None, {'slug': self.slug})

        def __str__(self):
            return self.title


    class Post(models.Model):
        title = models.CharField(max_length=127, unique=True)
        slug = models.SlugField(max_length=127, unique=True)
        body = models.TextField()
        posted = models.DateField(db_index=True, auto_now_add=True)
        category = models.ManyToManyField(Category)

        def get_absolute_url(self):
            return ('view_blog_post', None, { 'slug': self.slug })

        def __str__(self):
            return self.title

        def __repr__(self):
            return "<{0}: {1} - {2}>".format(__class__.__name__, self.title, self.posted)

In Django un modello è una classe Python che eredita da
``django.db.models.Model`` nel quale ogni attributo rappresenta una colonna
della tabella ed il modello la tabella stessa. In questo esempio abbiamo
definito due tabelle (in termini di database), La prima `Post`, per
memorizzare gli articoli e la seconda `Category` per le categorie.

Ogni Post ha cinque campi:

:title: rappresenta il titolo dell'articolo, di tipo *CharField*

:slug: rappresenta lo slug dell'articolo, di tipo *SlugField*

:body: rappresenta il test dell'articolo, di tipo *TextField*

:posted: la da di creazione dell'articolo, di tipo *DateField*

:category: il riferimento alla tabella che mappa i post alle categorie,
    di tipo *ManyToMany*

In questo modo abbiamo definito il modello della nostra applicazione, ma
il database è ancora vuoto. Per poter rendere effettive le modifiche dobbiamo:

    1. Creare le migrazioni
    2. Migrare

La creazione delle migrazioni avviene mediante l'esecuzione del comando

.. code-block:: bash

    python manage.py makemigrations blog

Che genera il seguente output

.. code-block:: bash

    Migrations for 'blog':
        blog/migrations/0001_initial.py
            - Create model Category
            - Create model Post

Ogni volta che modifichiamo i nostri modelli, dobbiamo dire a Django di
creare le migrazioni per riflettere le modifche. Le migrazioni sono il modo in
cui django memorizza i cambiamenti dei modelli (e quindi dello schema del
database).

Per rendere effettive le modifiche sulla base di dati, abbiamo bisogno di eseguire
il secondo comando:

.. code-block:: bash

    python manage.py migrate blog

Che genera il seguente output:

.. code-block:: bash

    Operations to perform:
        Apply all migrations: blog
    Running migrations:
        Applying blog.0001_initial... OK

.. todo:: finire la lista

    * Genera le tabelle nel database concatenando il nome del modello al nome
      dell'applicazione, così `Post` diventa `blog_post`
    * Le primary key (Gli ID) vengono generati in automatico (motivo per cui
      non le abbiamo definite durante la progettazione dello schema


Possiamo vedere la traduzione nel relativo DDL (Data Definition Language)

.. code-block:: SQL

    BEGIN;
    --
    -- Create model Category
    --
    CREATE TABLE "blog_category" (
        "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
        "title" varchar(127) NOT NULL,
        "slug" varchar(127) NOT NULL
    );
    --
    -- Create model Post
    --
    CREATE TABLE "blog_post" (
        "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
        "title" varchar(127) NOT NULL UNIQUE,
        "slug" varchar(127) NOT NULL UNIQUE,
        "body" text NOT NULL,
        "posted" date NOT NULL
    );
    --
    -- Create model Post-Category Many-To-Many
    --
    CREATE TABLE "blog_post_category" (
        "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
        "post_id" integer NOT NULL
            REFERENCES "blog_post" ("id") DEFERRABLE INITIALLY DEFERRED,
        "category_id" integer NOT NULL
            REFERENCES "blog_category" ("id") DEFERRABLE INITIALLY DEFERRED
    );
    CREATE INDEX "blog_category_title_b9fd2f90"
        ON "blog_category" ("title");
    CREATE INDEX "blog_category_slug_92643dc5"
        ON "blog_category" ("slug");
    CREATE INDEX "blog_post_posted_0a0f6719"
        ON "blog_post" ("posted");
    CREATE UNIQUE INDEX blog_post_category_post_id_category_id_189979f4_uniq
        ON "blog_post_category" ("post_id", "category_id");
    CREATE INDEX "blog_post_category_post_id_d7c84b08"
        ON "blog_post_category" ("post_id");
    CREATE INDEX "blog_post_category_category_id_e1f613f9"
        ON "blog_post_category" ("category_id");

    COMMIT;

Il DDL varia in base al database di backend specificato nelle
impostazioni del progetto, è l'ORM che ha il compito di tradurre i campi
specifici come **auto_increment** per MySQL, **serial** (PostgreSQL), oppure
**integer primary key autoincrement** (SQLite)

Per testare la nostra base dati, Django mette a disposizione una shell
interattiva che carica le impostazioni del progetto `settings.py`

.. code-block:: python

    python manage.py shell

Una volta entrati nella shell, risulta facile testare l'API del database

.. code-block:: python

    # Importiamo i modelli Post e Category
    >>> from blog.models import Post, Category

    # Facciamo una query al database per ritornare tutti i Post
    >>> Post.objects.all()
    <QuerySet []>

    # Il database non contiene alcun Post. Creiamo un Post.
    >>> Post.objects.create(
        title='First post',
        slug='first-post',
        body='My first post talks about how to write a blog!'
    )
    <Post: Post object (1)>

    # Otteniamo la data di creazione del Post
    >>> first_post.posted
    datetime.date(2018, 2, 19)

    # Otteniamo l'ID del Post
    >>> first_post.id
    1
    # Otteniamo il titolo del Post
    >>> first_post.title
    'First post'

    # Creiamo una categoria
    >>> news_category = Category.objects.create(title='News', slug='news')

    # Aggiungiamo il Post alla categoria appena creata
    >>> first_post.category.add(news_category)
    >>> first_post.save()

