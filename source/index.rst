.. tesi documentation master file, created by
   sphinx-quickstart on Sat Jan 13 18:31:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Sviluppo di una applicazione Web moderna: dall’idea al deploy con il framework Django
=====================================================================================

.. toctree::
    :maxdepth: 2
    :caption: Indice:

    02_django/00_django_index
    03_testing/00_testing_index
    04_deploy/00_deploy_index


Conclusioni e sviluppi futuri
=====================================================================================



Riferimenti
=====================================================================================


