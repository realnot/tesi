
Cos'e il Test-Driven Development
--------------------------------------------------------------------------------

Il TDD (sviluppo guidato dai test) è una metodologia di sviluppo del software
diversa da quella tradizionale. La prima domanda che ci poniamo è se il
progetto esistente, la funzione o il componente è scritta nel modo migliore
possibile ed in caso contrario effettuiamo un refactoring del codice per
modificare la parte del design relativa alla nuova funzionalità.

Invece di scrivere prima il codice funzionale e poi il test per verificarne la
solidità e il rispetto dei requisiti, si procede nel senso inverso.
Un programmatore che adotta un approccio TDD stretto, rifiuta di scrivere una
nuova funzione fino a quando non c'è un test che fallisce perché quella
funzione non è presente.

Sembra semplice in linea di principio adottare un approccio TDD, ma è facile
dimenticarsi e perdersi per strada, aggiungere una funzionalità, anche mentre
si sta riparando un bug. È un approccio che richiede pratica.

La domanda più comune è:"Come posso testare codice che ancora non esite?"

Il Test-Driven Development è un ciclo (Red-Green-Refactor)

    - Lo sviluppatore scrive il test relativo ad una funzionalità, che
      inizialmente fallisce poiché non ancora implementata.
    - Lo sviluppatore scrive codice funzionale per fare in modo che il test
      venga superato senza problemi.
    - Una volta ottenuti i risultati corretti, lo sviluppatore effettua un
      refactoring del codice, aggiungendo commenti, migliorando le performance
      mentre i test continuano ad essere superati.
    - Il programmatore ripete il ciclo fino a quando la funzionalità,
      il componente o il progetto non è terminato.


.. image:: ../_static/tdd_cycle.png
    :scale: 60 %
    :align: center

Il test iniziale dovrebbe essere scritto nel modo più semplice possibile,
poiché deve esprimere la logica che vogliamo implementare.

Nel capitolo tre abbiamo visto come creare una semplice applicazione, `un blog`.
Ora parleremo di come implementare un API mediante REST e poi di come testare
un endpoint.



