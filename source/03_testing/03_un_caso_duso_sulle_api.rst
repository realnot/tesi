
TDD - Un caso d'uso sulle API REST
--------------------------------------------------------------------------------

Per mostrare questo caso d'uso abbiamo bisogno di installare alcuni requisiti di
progetto quali `djangorestframework` e `markdown`. Il primo ci permette di
costruire una Wep API, il secondo di rendere l'API navigabile mediante browser ed
il terzo di mostrare tutte le URLs degli endpoints.

Installiamo le dipendenze

.. code-block:: bash

    pip install djangorestframework
    pip install markdown
    pip install django_extensions

Essendo `djangorestframework` e `django_exntesions` delle applicazioni Django, queste
vanno inserite nel file settings.py, nella lista `INSTALLED_APPS`

.. code-block:: python

    INSTALLED_APPS = [
        ...
        'rest_framework',
        'django_extensions',
    ]

Una volta aggiunte le applicazioni alla lista delle applicazioni installate
possiamo procedere con la creazione degli endpoint.

REST mediante Django Rest Framework
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

REST (REpresentational State Transfer) è un tipo di architettura software
per i sistemi distribuiti. L'architettura REST si basa su HTTP; il funzionamento
prevede una struttura degli URL ben definita (atta a identificare univocamente
una risorsa o un insieme di risorse) e l'utilizzo dei verbi HTTP specifici per
il recupero di informazioni (GET), per la modifica (POST, PUT, PATCH, DELETE)

.. [rest]_ https://en.wikipedia.org/wiki/Representational_state_transfer

Mediante REST siamo in grado ti creare le nostre API per il blog. Le API
(Application Program Interface) sono un interfaccia che permette di esporre
le funzionalità di un componente software all'esterno del nostro applicativo
per fare in modo che utenti o altri applicativi, possano usufruire dei servizi
che l'API espone.

Nel contesto del nostro blog, potremmo decidere di voler esporre agli utenti
la possibilità eseguire determinate operazioni su una o più risorse, permettendo
ai clients di poter effettuare le canoniche operazioni CRUD (Create, Read, Update,
Delete) sulla risorsa esposta, come ad esempio gli articoli.

All'interno dell'applicazione blog creiamo una struttura come la seguente:

::

    api
    ├── serializers.py
    ├── urls.py
    └── views.py

Lo scopo dei files all'interno della directory api è il seguente:

:serializers.py: il modulo che conterrà i serializer degli endpoints
:urls.py: il modulo per la configurazione delle urls degli endpoints
:views.py: il modulo per gestire la business logic degli endpoints

Prima di procedere alla spiegazione e implementazione dei moduli di cui sopra
dobbiamo fare in modo che le URLs degli endpoints siano aggiunte alle URLs
dell'applicazione blog. Per fare questo, apriamo il modulo urls.py ed aggiungiamo
una nuova url per raggiungere le API:

.. code-block:: python

    urlpatterns = [
        url(r'^$', views.index, name='index'),
        url(r'^(?P<slug>[\w-]+)/$', views.post_detail, name='post_detail'),
        url(r'^category/(?P<slug>[\w-]+)/$', views.category_detail, name='category_detail'),
        # ...
        url(r'^api/', include('blog.api.urls')), # carichiamo il file urls.py descritto sopra
    ]

In genere le URLs degli endpoints iniziano con la parola `api` (ed a volte anche
con una versione, /api/v1/, /api/v2/ ecc...) per distinguerle dal resto delle altre
URLs.

Adesso possiamo concentrarci sul file urls.py all'interno della directory `api`
(il file che abbiamo appena incluso) e scriviamo quanto segue.

.. code-block:: python

    from rest_framework.routers import DefaultRouter
    from . import views

    router = DefaultRouter()
    router.register(r'post', views.PostViewSet, base_name='post')

    urlpatterns = router.urls

Per prima cosa importiamo dalla librearia rest i routers e successivamente
da views.py le nostre views.

I routers sono un metodo molto comodo per generare in automatico le URLs
degli endpoints. Nell'esempio mostrato sopra abbiamo registrato un URL
`post`, la quale viene mappata alla view `PostViewSet` che come dice la parola
è un **ViewSet** con il nome `post`.

Grazie al pacchetto django_extensions installato in precedenza, possiamo
eseguire il comando:

.. code-block:: python

    ./manage.py show_urls | grep api/post

Per mostrare le URLs dell'endpoint appena creato. Vediamo l'output:

.. code-block:: bash

    (tesi) realnot@mars ~/Workspace/tesi/mysite $ ./manage.py show_urls | grep api/post
    /blog/api/post/	blog.api.views.PostViewSet	post-list
    /blog/api/post/<pk>/	blog.api.views.PostViewSet	post-detail
    /blog/api/post\.<format>/	blog.api.views.PostViewSet	post-list
    /blog/api/post/<pk>\.<format>/	blog.api.views.PostViewSet	post-detail

Come possiamo notare sono state create quattro URLs in automatico.

+--------------------------------+----------------------------+----------------+
| **URLs**                       | **ViewSet**                | **Nome**       |
+================================+============================+================+
| /blog/api/post/                | blog.api.views.PostViewSet | post-list      |
+--------------------------------+----------------------------+----------------+
| /blog/api/post/<pk>/           | blog.api.views.PostViewSet | post-detail    |
+--------------------------------+----------------------------+----------------+
| /blog/api/post\.<format>/      | blog.api.views.PostViewSet | post-list      |
+--------------------------------+----------------------------+----------------+
| /blog/api/post/<pk>\.<format>/ | blog.api.views.PostViewSet | post-detail    |
+--------------------------------+----------------------------+----------------+

Tralasciamo le ultime due URLs e concentriamoci sulle prime due. Grazie a queste
due URLs, siamo in grado di eseguire le operazioni CRUD sulla risorsa `post`.

L'endpoint senza primary key `/blog/api/post/` che di default prende il nome di
*post-list* permette ad esempio di eseguire una chiamata HTTP di tipo POST
per la creazione di una risorsa, oppure una chiamata GET per ottenere una lista
di risorse. L'endpoint è in grado di eseguire l'operazione corretta grazie al
verbo/metodo utilizzato sull'endpoint durante la chiamata.

Mostriamo un riepilogo delle operazioni possibili

+--------------------------+-------------------------+-------------------------+
| **Endpoint**             | **HTTP method**         | **REST action**         |
+==========================+=========================+=========================+
| /blog/api/post/          | GET                     | List                    |
+--------------------------+-------------------------+-------------------------+
| /blog/api/post/          | POST                    | Create                  |
+--------------------------+-------------------------+-------------------------+
| /blog/api/post/<pk>/     | GET                     | Retrieve                |
+--------------------------+-------------------------+-------------------------+
| /blog/api/post/<pk>/     | PUT                     | Update                  |
+--------------------------+-------------------------+-------------------------+
| /blog/api/post/<pk>/     | PATCH                   | Partial Update          |
+--------------------------+-------------------------+-------------------------+
| /blog/api/post/<pk>/     | DELETE                  | Delete                  |
+--------------------------+-------------------------+-------------------------+

Tutti queste operazioni si rendono disponibili grazie alla creazione di una sola
URL mediante i routers. Le URLs generate dal router vanno aggiunte alla lista
**urlspatterns** come mostrato nell'ultima riga dello snippet urls.py

Una volta creato il file urls.py, passiamo al file views.py e scriviamo il seguente
snippet:

.. code-block:: python

    from rest_framework import viewsets
    from ..api.serializers import PostSerializer
    from ..models import Post

    class PostViewSet(viewsets.ModelViewSet):
        queryset = Post.objects.all()
        serializer_class = PostSerializer

Il viewset non è altro che una classe che implementa i metodi GET, POST, PATCH,
ecc... sono uno strumento molto utilizzato in Django Rest perché grazie ad una
sola View permettono di intercettare tutte le requests sull'endpoint. La soluzione
alternativa sarebbe stata quella di creare una funzione / classe per ogni metodo
http. Sì sono ottimo strumento.

Il viewset preso in esame, ha due attributi.

:queryset: indica alla view la query da eseguire verso il database
:serializer_class: indica alla view la classe utilizzata per serializzare i dati

Da notare che la query verso il database è solo quella di `default`. Quando un
utente chiama un endpoint per ottenere un articolo specifico (quindi usando
una primary key) mediante il seguente endpoint: /blog/api/post/15, il viewset
invocherà il metodo `get_object()` che prenderà in automatico la primary key
dall'URL e ritornerà un oggetto serializzato in caso di match, altrimenti un
HTTP 404 Not Found. Se la chiamata fosse stata una GET per ottenere tutti gli
articoli, allora sarebbe stata eseguita la query di default.

La *serializer class* specificata, è quella definita all'interno del file
serializers.py. I serializer solo il modo in cui Django Rest Framework
processa i dati in ingresso ed in uscita dal mondo REST. Vediamo serializer.py
e spieghiamo brevemente la logica al suo interno.

.. code-block:: python

    from rest_framework import serializers
    from ..models import Post

    class PostSerializer(serializers.ModelSerializer):
        class Meta:
            model = Post
            fields = '__all__'

I serializer permettono di controllare come deve essere eseguita la serializzazione
di un oggetto Python. All'interno del serializer è presente una meta classe che
viene utilizzata per contenere e configurare il serializer (contiene appunto le
meta informazioni del serializer) e specifica le varie opzioni da eseguire per
serializzare/deserializzare gli oggetti. L'attributo **fields** impostato a '__all__'
specifica che tutti i campi del model `Post` verranno serializzati. Avremmo potutto
decidere di esporre solo alcune informazioni come titolo e corpo del post:

.. code-block:: python

    class PostSerializer(serializers.ModelSerializer):
        class Meta:
            # ...
            fields = ('title', 'body')

Oppure di escludere dalla serializzazione la data di creazione del post e serializzare
tutte le altre informazioni:

.. code-block:: python

    class PostSerializer(serializers.ModelSerializer):
        class Meta:
            # ...
            exclude = ('created',)


Alcuni campi possono essere configurati in sola lettura, come ad esempio tutti gli *Auto
fields* (tra cui le primary key), mediante l'attributo **read_only_fields**. Altri campi
invece ha senso configurarli in sola scrittura (utile per le password ad esempio)

.. code-block:: python

    class Meta:
        # ...
        extra_kwargs = {'password': {'write_only': True}}


Questi sono solo alcuni esempi delle infinite possibilità che il framework mette a
disposizione.

Non abbiamo parlato della validazione dei campi (singoli e dipendenti), di come
effettuare CRUD su tabelle many-to-many. Non abbiamo parlato di come sovrascrivere
i metodi `update()`, `create()` e `save()` per serializzare in modo customizzato
un singolo endpoint in base all'HTTP method. Anche i metodi `to_internal_value()` e
`to_representation()` meritano di essere citati, perché sono il modo in cui possiamo
alterare la serializzazione/deserializzazione. Questi argomento vanno oltre
gli scopi di questo documento.


Testare le API mediante Django Testing Framework
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Se puntiamo il browser all'indirizzo http://localhost:8000/blog/api/post/ sarà
possibile vedere la lista dei post del nostro blog.

.. image:: ../_static/rest_post_list.png
    :scale: 60 %
    :align: center

Ora creiamo una cartella tests che conterrà tutti i test dell'applicazione `blog`
e scriviamo alcuni test per l'API appena creata e scriviamo il modulo test_api.py


.. code-block:: python

    from rest_framework import status
    from rest_framework.test import APITestCase
    from rest_framework.reverse import reverse


    class APITests(APITestCase):

        def test_unauthenticated_client(self):
            response = self.client.get(reverse('post-list'))
            self.assertEqual(response.status_code, status.HTTP_200_OK, response.json())


Nel modulo test_api.py abbiamo creato una classe APITests che eredita da APITestCase,
la quale mette a disposizioni alcuni strumenti per testare le API, come un client che
ci permetterà di effettuare le varie chiamate all'API per svolgere i nostri tests.

Tutti i tests devono iniziare con la parola `test` per poter essere individuati correttamente
dal testing framework. È buona norma che il nome del test contenga una breve descrizione
di quello che vogliamo testare, in modo chiaro e conciso.

Nel test scritto sopra vogliamo testare ad esempio che per poter vedere la lista degli articoli
non è necessaria alcuna autenticazione al sistema, ovvero che una chiamata di tipo
get all'endpoint di nome `post-list` ritorni la lista degli articoli del nostro blog.

La funzione `reverse()` prende in ingresso il nome di un URL path e ritorna l'URL,
la quale sarà passata al metodo `get` dell'istanza `client` che effettuerà una chiamata
HTTP ti tipo get all'endpoint convertito dalla reverse().

Salviamo la risposta HTTP nella variabile `response` e mediante il metodo `assertEqual()` della
classe `TestCase`, sopraclasse di `APITestCase`, verifichiamo che lo **status code** della
risposta sia uguale allo stato HTTP 200 che identifica una risposta corretta.

La comparazione viene effettuata tra i primi due argomenti del metodo, il terzo argomento,
`response.json()` del tutto opzionale è utile per stampare a video l'errore in formato
JSON nel caso in cui venga lanciato un errore.

Proviamo ad eseguire il test mediante il comando

.. code-block:: bash

     ./manage.py test blog.tests.test_api

Che nel nostro caso dovrebbe generare un output simile al seguente

.. code-block:: bash

    Creating test database for alias 'default'...
    System check identified no issues (0 silenced).
    .
    ----------------------------------------------------------------------
    Ran 1 test in 0.012s

    OK
    Destroying test database for alias 'default'...
    (tesi) realnot@mars ~/Workspace/tesi/mysite $

Ogni volta che viene eseguito il test framework, Django esegue tutte le
migrazioni del database per poter partire da uno stato *pulito*. Questo
causa nella maggior parte dei casi problemi di performance, dovuti proprio
all'applicazione delle migrazioni, specialmente nei progetti con molte
applicazioni e modelli. Per risolvere questo problema possiamo usare
il parametro **--keepdb**

I test vengono eseguiti in modo casuale, inoltre ogni
test non conosce nessuna informazioni degli altri test proprio perché
i test sono studiati per essere indipendenti gli uni dagli altri.

Supponiamo ora di voler fare in modo che la lista dei posts sia accessibile
solo previa autenticazione dal parte del client. Per prima cosa editiamo
il modulo test_api.py e verifichiamo che l'HTTP status code ritorni
**403 Forbidden**

.. code-block:: python

    self.assertEqual(response.status_code, status.status.HTTP_403_FORBIDDEN, response.json())

Successivamente lanciamo il test e verifichiamo che questo fallisca.

.. code-block:: bash

    (tesi) realnot@mars ~/Workspace/tesi/mysite $ ./manage.py test blog.tests.test_api --keepdb
    Using existing test database for alias 'default'...
    System check identified no issues (0 silenced).
    F
    ======================================================================
    FAIL: test_unauthenticated_client (blog.tests.test_api.APITests)
    ----------------------------------------------------------------------
    Traceback (most recent call last):
      File "/home/realnot/Workspace/tesi/mysite/blog/tests/test_api.py", line 11, in test_unauthenticated_client
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, response.json())
    AssertionError: 200 != 403 : []

    ----------------------------------------------------------------------
    Ran 1 test in 0.012s

    FAILED (failures=1)
    Preserving test database for alias 'default'...

Come possiamo vedere il metodo assertEqual() si aspetta uno status code 403 (come abbiamo specificato)
ma invece ritorna 200. Questo significa che l'endpoint è accessibile anche ai clients
non autenticati. Procediamo ora editando il modulo views.py, in particolare il viewset affinché
permetta l'accesso solo a client autenticati.

.. code-block:: python

    class PostViewSet(viewsets.ModelViewSet):
        permission_classes = (IsAuthenticated,) # <--- solo client autenticati
        queryset = Post.objects.all()
        serializer_class = PostSerializer

Alla classe abbiamo aggiunto l'attributo `permission_classes` che prende come valore
una tupla contenente le sottoclassi di BasePermissions.

Ora proviamo ad eseguire nuovamente il test e vediamo che tutto funzioni come previsto

.. code-block:: bash

    (tesi) realnot@mars ~/Workspace/tesi/mysite $ ./manage.py test blog.tests.test_api --keepdb
    Using existing test database for alias 'default'...
    System check identified no issues (0 silenced).
    .
    ----------------------------------------------------------------------
    Ran 1 test in 0.012s

    OK
    Preserving test database for alias 'default'...

Siamo partiti con una specifica in mente, volevamo che l'endpoint fosse raggiungibile
solo previa autenticazione. Abbiamo modificato il test affinché lanciasse un'eccezione
nel caso in cui l'endpoint fosse raggiungbile senza autenticazione. Abbiamo rieseguito
il test ed abbiamo visto il suo fallimento. Il test era corretto quindi siamo andati a
modificare la porzione di codice che rispecchiasse il comportamente voluto nel test.
Abbiamo aggiunto una permission class nel viewset e successivamente rieseguito nuovamente
il test, questa volta con esito positivo.

Nel test precedente volevamo testare che soltanto i clients autenticati potessero vedere
i post del nostro blog. Ora proviamo a complicare ulteriormente la logica e proviamo
a testare che solo un utente registrato e autenticato possa creare un post nel nostro
blog. Le cose si complicano perché nei test il database è completamente vuoto e questo
significa che non esiste alcun utente, post e qualsiasi altro dato. Nello specifico
dobbiamo compiere le seguenti azioni

    1. Dobbiamo prima di tutto creare un utente
    2. Autenticare l'utente nella piattaforma
    3. Creare un dizionario che simuli il popolamento di un form.
    4. Eseguire una chiamata all'endpoint mediante il metodo `POST` del protocollo HTTP
    5. Verificare lo status code dell'HTTP response.
    6. Verificare i dati della response
    7. Interrogare il database e verificare che il form serializzato corrisponda
       al record presente nel database

I passi 1 e 2 in genere vengono ripetuti per molti tests, quindi conviene fattorizzare
la logica in modo tale che ogni test parta con uno stato (l'esistenza di un utente e
l'autenticazione dello stesso alla piattaforma). Per fare questo la classe UnitTest di python
mette a disposione un metodo chiamato setUp(), il quale verrà invocato prima dell'esecuzione
di ogni singolo test per eseguire la logica al suo interno (si comporta come un costruttore,
inizializzando i tests)

Apriamo ora nuovamente il file test_api.py e scriviamo il test descritto sopra, implementando
per prima cosa il metodo setUp()

.. code-block:: python

     def setUp(self):
        # Creiamo un utente
        self.user = User.objects.create_user(
            username='john', email='john@snow.com', password='12345'
        )
        # Autentichiamo l'utente
        self.client.login(username='john', password='12345')

User è un model (facente parte dell'Authentication Framework di Django). Abbiamo invocato
il metodo create_user() del manager di default che prende il nome di **objects**. A questo
punto self.user contiene una istanza dell'utente creato e salvato nel database. Il `client`
messo a disposizione dal testing framework rende disponibile un metodo login() che sfrutta
i backend di autenticazione (in ordine di configurazione, definiti nel modulo settings.py)
per autenticare il client.

A questo punto abbiamo un utente valido e autenticato, possiamo procedere a creare il test.

.. code-block:: python

    def test_create_new_post(self):
        form_data = {
            'title': 'post title',
            'slug': 'post-title',
            'body': 'post created through API rest',
        }
        response = self.client.post(reverse('post-list'), form_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.json())

Nel listato sopra abbiamo creato un dizionario il quale contiene tutti i dati che saranno
inviati medianto POST all'endpoint di nome post-list. Vedete che il nome **post-list** è
rimasto invariato rispetto al primo test? L'URL rimane la stessa, ma cambia il metodo HTTP
(prima era una chiamata di tipo GET, mentre ora una chiamata di tipo POST)

Dopo aver effettuato la chiamata, verifichiamo che lo status code della response sia
un 201, ovvero lo stato HTTP che ritorna una POST di successo. Lanciamo il test framework.


.. code-block:: bash

    (tesi) realnot@mars ~/Workspace/tesi/mysite $ ./manage.py test blog.tests.test_api --keepdb
    Using existing test database for alias 'default'...
    System check identified some issues:

    System check identified 1 issue (0 silenced).
    F
    ======================================================================
    FAIL: test_create_new_post (blog.tests.test_api.APITests)
    ----------------------------------------------------------------------
    Traceback (most recent call last):
      File "/home/realnot/Workspace/tesi/mysite/blog/tests/test_api.py", line 29, in test_create_new_post
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.json())
    AssertionError: 400 != 201 : {'category': ['This field is required.']}

    ----------------------------------------------------------------------
    Ran 1 test in 0.223s

    FAILED (failures=1)
    Preserving test database for alias 'default'...

Dall'esecuzione del test notiamo un risultato inatteso, lo status code della response
non è un 201, ma un 400. Quando abbiamo creato il modello `Post`, abbiamo
definito il campo category come obbligatorio, ma poi non abbiamo passato
l'informazione nel form data durante la chiamata. Questo comportamento
potrebbe essere quello desiderato, nessuno ci vieta di impostare tale campo
come obbligatorio. Dipende sempre dagli obiettivi che c'eravamo prefissati e dalle
specifiche di progetto. D'altro canto però potremmo decidere che un post potrebbe
non appartenere a nessuna categoria e liberare dal questo vincolo il modello.
In questa sede optiamo per la seconda opzione. Modificamo il model e creiamo le
opportune migrazioni affinché le modifiche si riflettano sul database di backend.

.. code-block:: python

    class Post(models.Model):
        title = models.CharField(max_length=127, unique=True)
        slug = models.SlugField(max_length=127, unique=True)
        # ...
        category = models.ManyToManyField(Category, blank=True) # <-- blank


Prima generiamo le migrazioni

.. code-block:: bash

    (tesi) realnot@mars ~/Workspace/tesi/mysite $ ./manage.py makemigrations

    Migrations for 'blog':
      blog/migrations/0002_auto_20180304_1524.py
        - Alter field category on post

E successivamente le applichiamo

.. code-block:: bash

    (tesi) realnot@mars ~/Workspace/tesi/mysite $ ./manage.py migrate

    Operations to perform:
      Apply all migrations: admin, auth, blog, contenttypes, sessions
    Running migrations:
      Applying blog.0002_auto_20180304_1524... OK

Ora è il momento di eseguire nuovamente i test:

.. code-block:: bash

    (tesi) realnot@mars ~/Workspace/tesi/mysite $ ./manage.py test blog.tests.test_api --keepdb
    Using existing test database for alias 'default'...
    System check identified no issues (0 silenced).
    .F
    ======================================================================
    FAIL: test_unauthenticated_client (blog.tests.test_api.APITests)
    ----------------------------------------------------------------------
    Traceback (most recent call last):
      File "/home/realnot/Workspace/tesi/mysite/blog/tests/test_api.py", line 20, in test_unauthenticated_client
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, response.json())
    AssertionError: 200 != 403 : []

    ----------------------------------------------------------------------
    Ran 2 tests in 0.450s

    FAILED (failures=1)
    Preserving test database for alias 'default'...

Cosa è successo? L'aver autenticato il client nel metodo setUp() (eseguito per ogni test)
ha fatto fallire il test `test_unauthenticated_client`. Il metodo assertEsequal() si aspettava un 403
invece, essendo il client autenticato, lo status code della response ottenuta è un 200.
Poco male, questa volta agiamo sul test, perché effettivamente è quello che vogliamo verificare
ovvero che un client autenticato possa vedere la lista dei post. Possiamo prendere due strade

    1. Deautentichiamo il client prima di effettuare la chiamata
    2. Modifichiamo lo status code da verificare nel metodo assertEqual()

Dato il nome del test (e quello che c'eravamo prefissati di testare), è preferibile mantere
lo status code 403 e deautenticare il client per mettere ancora più in evidenza che un
client non autenticato, ottiene un 403 come status code dell'HTTP response.

.. code-block:: python

    def test_unauthenticated_client(self):
        self.client.logout()
        # ...

Lanciamo i test

.. code-block:: bash

    (tesi) realnot@mars ~/Workspace/tesi/mysite $ ./manage.py test blog.tests.test_api --keepdb
    Using existing test database for alias 'default'...
    System check identified no issues (0 silenced).
    ..
    ----------------------------------------------------------------------
    Ran 2 tests in 0.454s

    OK
    Preserving test database for alias 'default'...

Questa volta tutto è andato a buon fine. Siamo sicuri che solamente un client autenticato
sia in grado di creare e visualizzare gli articoli. Proseguiamo i nostri test, e verifichiamo
che il form passato dal client nella chiamata, contenga gli stessi campi e valori di una istanza
di un recordo recuperata dal database.


.. code-block:: python

    def test_create_new_post(self):
        form_data = {
            'title': 'post title',
            'slug': 'post-title',
            'body': 'post created through API rest',
        }
        # ..
        obj = Post.objects.get(pk=response.data.get('id'))
        data = PostSerializer(obj).data
        for key, value in response.data.items():
            self.assertIn(form_data.values(), data.values())

Spieghiamo in dettaglio cosa stiamo facendo. Per prima cosa recuperiamo dalla response
che ha status code 200, il valore dell'ID dell'oggetto appena creato. Grazie a questo
ID possiamo effettuare una query al database per recuperare l'istanza esatta del Post
che successivamente viene assegnata alla variabile `obj`.

Ora utilizziamo il serializer per trasformare l'istanza in un dizionario (serializzando e
validando anche i campi come date e password che nel database hanno una rappresentazione
diversa).

Dopo aver serializzato l'istanza, verifichiamo che tutti i valori del form_data siano
uguali a quelli dell'istanza. I dati passati all'API corrispondono esattamente ai dati
presenti nel database.

Come spiegato inizialmente, il TDD è un processo iterativo, un ciclo. Dobbiamo avere
in mente cosa vogliamo ottenere, scriviamo un test per ottenere quella determinata
funzione, eseguiamo il test, analizziamo la risposta, implementiamo la funzione,
fattorizziamo il codice e ripetiamo il processo.

Avremmo potuto testare tutto il CRUD, testare il processore di registrazione di un utente,
testare diversi sistemi di autenticazione come JWT oppure OAuth2, ma questo va oltre gli
scopi di questo documento. Quello che volevo mettere in luce è il processo e l'impostazione,
prima di tutto mentale, di come agire quando vogliamo testare un componente, un applicativo.

Esistono molteplici tipi di test, quelli introdotti in questo capitolo rientrano nella categoria
degli UnitTest. In genere gli UnitTest si occupano di testare il componente nei casi limite
(a meno infinito e più infitito) e nel mezzo, per verificare come reagisce il componente e
e se il risultato è quello atteso.

Cosa bisogna testare? In genere tutto il codice che scriviamo noi (non quello che mette a
disposizione il framework o altri packages esterni). Ho voluto mostrare alcuni esempi sulle
API mentre ho introdotto brevemente REST, evitando di testare in questo modo i modelli, le views ed
il resto dei moduli principali.

I test sono un argomento molto vasto, ma altrettanto importante. Testare ti permette di capire
meglio di qualsiasi altro compito come funzianano le meccaniche piuttosto che aggiungere
semplicemente features.