
Deploy
--------------------------------------------------------------------------------

La nostra applicazione vive attualmente sulla nostra macchina di sviluppo, gli articoli
del nostro blog scritti maestosamente bene non saranno visti dal mondo esterno. Per
poter fare in modo di condividere la nostra applicazione e renderla quindi fruibile,
dobbiamo renderla visibile nel WWW (World Wide Web).

Per fare questo abbiamo diverse possibilità, la prima fra tutte, nolleggiare un VPS [#tGgB]_
(Virtual Private Server) tramite uno dei provider che offrono questo tipo di
servizi, come ovh.com oppure contabo.com giusto per citarne alcuni, oppure, optare
per il cloud [#kMBI]_.

VPS hosting vs Cloud Hosting
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In realtà potremmo anche decidere di optare per l'acquisto di un server da tenere in casa/
azienda oppure da portare in una software house. L'acquisto della macchina ha alcuni benefici,
tra i quali:

    :hardware: possiamo optare per la configurazione che più rispecchia le nostre
        necessità, abbatendo in questo modo i costi (a parità di configurazione)
        rispetto al nolleggio presso il service provider. Il problema è che se
        decidiamo di tenerlo in casa la banda necessaria per gestire il traffico
        dati non sarà sufficiente per progetti di piccole/medie dimensioni. Un'altra
        soluzione potrebbe essere quella di installare il server presso il service
        provider esternalizzando i costi di gestione come la banda e l'energia
        elettrica, garantendo al contempo scalabilità per quanto riguarda la banda.

    :software: essendo la macchina nostra, possiamo decidere di utilizzare qualsiasi
        sistema operativo, configurando la macchina su misura in base alle nostre
        necessità.

Il limite più grosso derivante dall'acquisto della macchina è dovuto agli elevati
costi di gestione nelle risorse impiegate:

    :accessibilità: Garantire un elevato tasso di uptime per fare in modo che il servizio
        sia sempre raggiungibile.

    :sicurezza: Proteggere l'infrastruttura di rete dagli attacchi degli hacker.

    :performance: ottimizzare le performance controllando le risorse gestendo
        il load balacing

    :scalabilità: non solo per quanto riguarda l'hardware, ma anche per il software,
        specialmente sulla base di dati. Lo sharding del database, sia verticale
        che orizzontale non è un compito da prendere alla leggera.

Tutte queste operazioni richiedono l'intervento di diverse figure altamente specializzate,
come database administrators, security team, network engineers, devops e così via. Mantenere
in piedi una infrastruttra non è semplice. Il VPS risolve in parte questi problemi,
grazie anche alle configurazioni da remoto ed alla gestione di alcuni task automatizzati,
tuttavia non è scalabile.

Il cloud elimina tutti questi problemi, garantendo elevate perfomance e scalabilità,
facilitando la gestione dei progetti ed esternalizzando tutti i costi. Inoltre il
modello di pagamento varia, a differenza di quanto avviene nei VPS, dove il canone di
pagamento rimane inalterato, nel Cloud paghi solo le risorse realmente impiegate.

Riepiloghiamo in breve le differenze

+-----------------------------+-----------------------------+-----------------------------+
|                             | Cloud Hosting               | VPS Hosting                 |
+=============================+=============================+=============================+
| Scalabilità                 | Sì                          | No                          |
+-----------------------------+-----------------------------+-----------------------------+
| Modello di pagamento        | Paghi le risorse impiegate  | Pagamento mensile/annuale   |
+-----------------------------+-----------------------------+-----------------------------+
| Upgrade delle risorse       | Istantaneo                  | Richiede tempo (limitato)   |
+-----------------------------+-----------------------------+-----------------------------+
| Scelta dell'infrastruttura  | Disponibile                 | Non possibile               |
+-----------------------------+-----------------------------+-----------------------------+
| Costi                       | Economico nei piccoli       | Abbastanza contenuti e      |
|                             | progetti, costosono         | relativamente bassi         |
|                             | non appena il sistema scala |                             |
+-----------------------------+-----------------------------+-----------------------------+
| Scelta dell'OS              | Puoi sceglere qualunque     | Dipende dal provider        |
|                             | sistema                     |                             |
+-----------------------------+-----------------------------+-----------------------------+

Alcuni esempi di Cloud Hosting sono Google App Engine, Amazon AWS, Heroku, Digital Ocean,
giusto per citarne alcuni.

Deploy: Heroku
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Prima di poter fare il deploy su Heroku, dobbiamo creare un account. Una volta
creato l'account, possiamo sceglere tra diversi tipi di piani:

    * Free
    * Hobby
    * Standard
    * Performance

Per effettuare il deploy del nostro blog, partiremo con il piano `Free` (Scalare è
molto semplice). Il primo passo è quello di installare la CLI [#RJZK]_ (Command Line Interface)
di Heroku, strumento messo a disposizione da Heroku, per gestire le nostre applicazioni.
È possibile effettuare il download della CLI tramite direttamente da Heroku oppure
tramite npm [#XkCe]_ (Node Package Manager).

.. code-block:: bash

    npm install -g heroku-cli

Ho effettuato il download tramite npm poiché attualmente per Gentoo Linux (la macchina
utilizzata per lo sviluppo) non è disponibile lo script.

Dopo aver recuperato il pacchetto, procediamo effettuando il login alla piattaforma
mediante CLI.

.. code-block:: bash

    $ heroku login
    Enter your Heroku credentials:
    Email: mauro.crociara@student.unife.it
    Password: *****
    Logged in as mauro.crociara@student.unife.it

Una volta effettuato il login, creiamo l'applicazione `blog`

.. code-block:: bash

    $ heroku create tesi-blog
    Creating tesi-blog... done
    https://tesi-blog.herokuapp.com/ | https://git.heroku.com/tesi-blog.git

Per poter effettuare il deploy, dobbiamo definire alcuni requisiti per la
produzione. Per fare questo usiamo il comando pip freeze che prende
tutti i pacchetti installati nell'ambiente virtuale (compresi di versione)
e tramite la redirezione dell I/O creiamo il file requirements.txt.

.. code-block:: bash

    pip freeze > requirements.txt

Una volta aggiunti i requisiti possiamo procedere lanciando l'applicazione

.. code-block:: bash

    $ heroku local web
    9:11:08 PM web.1 |  [2018-03-05 21:11:08 +0100] [27327] [INFO] Starting gunicorn 19.7.1
    9:11:08 PM web.1 |  [2018-03-05 21:11:08 +0100] [27327] [INFO] Listening at: http://0.0.0.0:5000 (27327)
    9:11:08 PM web.1 |  [2018-03-05 21:11:08 +0100] [27327] [INFO] Using worker: sync
    9:11:08 PM web.1 |  [2018-03-05 21:11:08 +0100] [27330] [INFO] Booting worker with pid: 27330

Se in locale funziona tutto correttamente, possiamo procedere a caricare il codice
su heroku tramite una operazione di `push`

.. code-block:: bash

    $ git push heroku master

Se tutto è andato a buon fine, il nostro blog sarà disponibile all'indirizzo web
https://tesi-blog.herokuapp.com/

Bisogna considerare alcune semplificazioni per quanto riguarda il database di backend.
avendo utilizzato SQLite non è stato necessario effettuare alcuna migrazione dopo
aver `pushato` il codice in remote. Il nostro blog non faceva uso di file statici
come immagini, javascript e css per cui non abbiamo dovuto utilizzare whitenoise per
servirli.


.. [#tGgB] Virtual Private Server - https://en.wikipedia.org/wiki/Virtual_private_server
.. [#kMBI] Cloud Computing - https://en.wikipedia.org/wiki/Cloud_computing
.. [#RJZK] Command Line Interface - https://en.wikipedia.org/wiki/Command-line_interface
.. [#XkCe] Node Package Manager - https://www.npmjs.com/
