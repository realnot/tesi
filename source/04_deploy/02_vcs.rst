
VCS (Version Control System)
--------------------------------------------------------------------------------

Introduzione
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Prima di procedere con il deploy della nostra applicazione, introduciamo
brevemente il concetto di VCS (Version Control System) [#NkxD]_ e perché è
importante per qualsiasi progetto e dimensione del team di sviluppo.

Il VCS è un sistema che memorizza i cambiamenti di un file o un insieme
di file nel tempo, in modo che sia possibile ripristinare una determinata
versione successivamente. Capita molte volte di lavorare ad un progetto
e dopo aver effettuato diverse modifiche accorgersi che lo stato raggiunto
non è precisamente quello che volevamo ottenere. Grazie al controllo
di versione sarà possibile ripristinare lo stato precedente, come fosse
una macchina del tempo. L'importanza di utilizzare un VCS non è solo
come mero strumento di backup, ma permette di verificare lo stato attuale
del progetto o vedere chi ha contribuito ad una precisa feature. Anche durante
la stesura di questo documento è stato utilizzato un VCS (in particolare
DVCS [#nCrD]_) perché non potevamo permetterci di perdere una qualunque modifica.

In particolare è importante usare il controllo di versione per le seguenti ragioni:

    * Confrontare due versione diverse dello stesso file/codice
    * Condividere il codice con altri sviluppatori/colleghi
    * Rivedere tutta la storia del progetto
    * Ripristinare il progetto in caso di perdita
    * Ripristinare il codice ad una versione precedente se qusto ha introdotto
      dei bug o non è stato scritto del modo più chiaro ed efficiente possibile
    * Vedere quanto lavoro è stato fatto fino a quel momento
    * Mantenere versioni differenti di uno stesso prodotto
    * Velocizzare la messa in produzione del progetto, quindi effettuare
      CI (Continuos Integration) / CD (Continuos Delivery) / CD (Continuos Deployment).
      **Lucas Bärenfänger** ha spiegato in modo breve e conciso in un articolo [#TsXJ]_
      pubblicato su stackoverflow.com in cosa consistono e perché sono importanti per
      lo sviluppo di un progetto.

Questi sono solo alcuni dei benefici apportati dall'utilizzo di un VCS. Successivamente,
quando mostreremo come effettuare il deployment vedremo come sia semplice mettere
il progetto in produzione grazie al VCS. Esistono diverse tipologie di VCS che introduciamo
brevemente qui sotto.

    :Local Version Control Systems: consiste nella copia del file/progetto in directory
        differenti, mantenute localmente sulla machina di sviluppo. Questo approccio è
        ancora utilizzato poiché semplice da effettuare, tuttavia si rileva `error-prone`.
        può capitare di trovarsi all'interno della directory/file sbagliato e apportare
        quindi le dovute modifiche nel punto sbagliato, compromettendo il file stesso e
        qualche volta anche il progetto.

    :Centralized Version Control Systems: consiste nell'avere il progetto in un server
        centrale, condiviso con i colleghi di lavoro. Ogni sviluppatore può apportare le
        dovute modifiche al progetto e poi caricarle sul server in modo che i propri colleghi
        possano scaricare e integrare il contributo di ogni altro developer. Questo tipo
        di VCS permette di lavorare in modo collaborativo e di migliorare i flussi di lavoro,
        ma presenta un grosso difetto. Nel caso in cui il server non fosse raggiungibile
        o subisse un guasto hardware, non sarebbe più possibile ripristinare il progetto.
        Questo sistema è affetto dal cosidetto SPOF (Single Point of Failure) [#YQOF]_

    :Distributed Version Control Systems: per eliminare il SPOF sono nati i sistemi
        distributi. Ogni developer o membro del team di sviluppo ha una copia in locale
        di tutto il progetto (repository) in modo che se il server non fosse raggiungibile
        per le ragione elecante sopra, sarà possibile ripristinare lo stato antecedente
        al guasto. Grazie a questo sistema è possibile inoltre lavorare in modo più
        collaborativo, mediante diverse tipologie di `workflows` al contrario di quanto
        avviene nei sistemi centralizzati.

Nella prossima sezione vedremo come usare un DVCS e creare un repository.

.. [#NkxD] Getting started about Version Control - https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control
.. [#nCrD] Distribute Version Control - https://en.wikipedia.org/wiki/Distributed_version_control
.. [#TsXJ] Continuos Integration vs Continuos Delivery vs Continuos Deployment https://stackoverflow.com/questions/28608015/continuous-integration-vs-continuous-delivery-vs-continuous-deployment#28628086
.. [#YQOF] Single Point of Failure - https://en.wikipedia.org/wiki/Single_point_of_failure


Sistema di controllo di versione distribuito: Bitbucket
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Bitbucket [#QMdc]_ è un servizio di hosting per progetti che usano sistemi di controllo di
versione e dispone di un'interfaccia web semplice e intuitiva. Il servizio offre account sia
gratuiti che a pagamento in base alle esigenze. Bitbucket supporta sia Mercurial [#bvNK]_
(fin dal lancio del prodotto) che `Git` [#MKof]_ (dal 2011) che sono attualmente tra i sistemi
di controllo di versione distribuiti tra i più usati.

Andiamo su bitbucket.org e dopo esserci registrati (il processo di registrazione esula dagli
scopi di questo documento) creiamo un repository. Per creare un repository basterà utilizzare
il menu sulla sinistra e clicclare sul simbolo `+` (più) oppure puntare il browser al
seguente indirizzo: https://bitbucket.org/repo/create

L'interfaccia web dovrebbe essere simile alla seguente

.. image:: ../_static/bitbucket_create_repo.png
    :scale: 50 %
    :align: center

Una volta creato il repository, apriamo un termiale ed eseguiamo i seguenti comandi:

.. code-block:: bash

    git clone git@bitbucket.org:realnot/blog.git
    cd blog
    echo "# My blog's README" >> README.md
    git add README.md
    git commit -m "Initial commit"
    git push -u origin master

Abbiamo clonato il repository in locale e siamo entrati nella cartella del respository.
Seccessivamente abbiamo creato un file README.md ed infine abbiamo aggiunto tale file alla
lista dei file che devono essere tracciati dal sistema di controllo di versione Git.
Una volta aggiunto il file README.me, commentiamo l'operazione e carichiamo quest'ultimo
su Bitbucket. Lo scopo del file README.md è quello di introdurre il progetto.
All'interno del file vengono generalemente aggiunte le seguente informazioni

    * Autore del progetto
    * Descrizione del progetto
    * Come utilizzare il progetto
    * Come contribuire al progetto

Noi ci siamo limitati ad un breve commento. L'utilizzo del file README.md non è
obbligatorio, anche se in gengere i progetti con un buon README, sono quelli che ottengono
più contributi dalla comunità.

Addesso copiamo il progetto dalla precedente posizione all'interno della cartella blog
e lanciamo i seguenti comandi:

.. code-block:: bash

    cp -r ../tesi/mysite .
    git add mysite
    git commit -m "Added initial version of the project to repository"
    git push -u origin master

Cosa è successo? Abbiamo copiato la cartella del progetto Django `mysite` all'interno
del repository `blog` mediante il comando `cp -r ../tesi/mysite .`. Abbiamo aggiunto
l'intero progetto al controllo di versione che ora terrà traccia di ogni file
presente all'interno di `mysite` mediante il comando `git add`. Abbiamo commentato
l'operazione mediante il comando `git commit` ed infine abbiamo caricato su bitbucket
le modifiche apportate mediante il comando `git push -u origin master`. Ora il nostro blog
è completamente online, al sicuro, su bitbucket.

Quando eseguiamo il comando `git add *` quest'ultimo carica qualsiasi tipo di file all'interno
della directory `mysite` a meno che non venga specificato un singolo file o una serie di file.
Per controllare in modo automatizzato cosa caricare e cosa ignorare è possibile creare
un file di nome `.gitignore` [#WtWx]_ con all'interno alcune regole. Git, prima di aggiungere
i file al controllo di versione, leggerà il file .gitingnore e se qualche regola dovesse
corrispondere ad uno o più file che stiamo cercando di caricare, quest'ultimi verranno
semplicemente ignorati. Su github.com è disponile una serie ti templates [#sqFb]_ per .gitignore.


.. [#QMdc] Bitbucket - https://bitbucket.org/
.. [#bvNK] Mercurial - https://git-scm.com/
.. [#MKof] Git - https://www.mercurial-scm.org/
.. [#WtWx] Git ignore - https://git-scm.com/docs/gitignore
.. [#sqFb] Git ignore collection - https://github.com/github/gitignore
