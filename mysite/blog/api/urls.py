# -*- coding: utf-8 -*-

from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
router.register(r'post', views.PostViewSet, base_name='post')

urlpatterns = router.urls
