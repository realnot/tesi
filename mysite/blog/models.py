# -*- coding: utf-8 -*-
from django.db import models


# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=127, db_index=True)
    slug = models.SlugField(max_length=127, db_index=True)

    def get_absolute_url(self):
        return ('view_blog_category', None, {'slug': self.slug})

    def __str__(self):
        return self.title


class Post(models.Model):
    title = models.CharField(max_length=127, unique=True)
    slug = models.SlugField(max_length=127, unique=True)
    body = models.TextField()
    posted = models.DateField(db_index=True, auto_now_add=True)
    category = models.ManyToManyField(Category, blank=True)

    def get_absolute_url(self):
        return ('view_blog_post', None, { 'slug': self.slug })

    def __str__(self):
        return self.title

    def __repr__(self):
        return "<{0}: {1} - {2}>".format(__class__.__name__, self.title, self.posted)