from django.shortcuts import render

# Create your views here.
from django.shortcuts import get_object_or_404

from .models import Post, Category


def index(request):
    posts = Post.objects.all()[:5]
    return render(request, 'blog/index.html', {'posts': posts})


def post_detail(request, slug):
    post = get_object_or_404(Post, slug=slug)
    return render(request, 'blog/post_detail.html', {'post': post})


def category_detail(request, slug):
    category = get_object_or_404(Category, slug=slug)
    return render(request, 'blog/category_detail.html', {'category': category})