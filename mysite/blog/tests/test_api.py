from django.contrib.auth.models import User

from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse

from ..api.serializers import PostSerializer
from ..models import Post

class APITests(APITestCase):

    def setUp(self):
        # Creiamo un utente
        self.user = self.user = User.objects.create_user(
            username='john', email='john@snow.com', password='12345'
        )
        # Autentichiamo l'utente
        self.client.login(username='john', password='12345')

    def test_unauthenticated_client(self):
        self.client.logout()
        response = self.client.get(reverse('post-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, response.json())

    def test_create_new_post(self):
        form_data = {
            'title': 'post title',
            'slug': 'post-title',
            'body': 'post created through API rest',
        }
        response = self.client.post(reverse('post-list'), form_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.json())

        obj = Post.objects.get(pk=response.data.get('id'))
        data = PostSerializer(obj).data
        for key, value in response.data.items():
            self.assertIn(value, data.values())