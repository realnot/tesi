
from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<slug>[\w-]+)/$', views.post_detail, name='post_detail'),
    url(r'^category/(?P<slug>[\w-]+)/$', views.category_detail, name='category_detail'),
    url(r'^api/', include('blog.api.urls')),
]
