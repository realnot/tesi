\select@language {italian}
\select@language {italian}
\contentsline {chapter}{\numberline {1}Django Framework}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Cos\IeC {\textquoteright }\IeC {\`e} Django?}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Come funziona Django?}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Creazione dell\IeC {\textquoteright }ambiente di sviluppo}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Definire e gestire i requisiti}{4}{section.1.4}
\contentsline {section}{\numberline {1.5}Creare un progetto}{5}{section.1.5}
\contentsline {section}{\numberline {1.6}Specificare il database di backend}{6}{section.1.6}
\contentsline {section}{\numberline {1.7}Object Relational Mapping (ORM)}{7}{section.1.7}
\contentsline {section}{\numberline {1.8}Server di sviluppo}{7}{section.1.8}
\contentsline {section}{\numberline {1.9}Creazione di un\IeC {\textquoteright }applicazione}{8}{section.1.9}
\contentsline {section}{\numberline {1.10}MVT (Model View Template) pattern}{9}{section.1.10}
\contentsline {section}{\numberline {1.11}Django: Model}{9}{section.1.11}
\contentsline {section}{\numberline {1.12}Django: Views}{12}{section.1.12}
\contentsline {section}{\numberline {1.13}Django: Template}{15}{section.1.13}
\contentsline {section}{\numberline {1.14}DTL (Django Template Language)}{16}{section.1.14}
\contentsline {subsection}{\numberline {1.14.1}Le variabili}{16}{subsection.1.14.1}
\contentsline {subsection}{\numberline {1.14.2}I filtri}{16}{subsection.1.14.2}
\contentsline {subsection}{\numberline {1.14.3}I Tag}{16}{subsection.1.14.3}
\contentsline {subsection}{\numberline {1.14.4}L\IeC {\textquoteright }ereditariet\IeC {\`a}}{17}{subsection.1.14.4}
\contentsline {chapter}{\numberline {2}TDD (Test-Driven Development)}{19}{chapter.2}
\contentsline {section}{\numberline {2.1}Cos\IeC {\textquoteright }e il Test-Driven Development}{19}{section.2.1}
\contentsline {section}{\numberline {2.2}TDD - Un caso d\IeC {\textquoteright }uso sulle API REST}{20}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}REST mediante Django Rest Framework}{21}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Testare le API mediante Django Testing Framework}{23}{subsection.2.2.2}
\contentsline {chapter}{\numberline {3}Deploy}{31}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduzione}{31}{section.3.1}
\contentsline {section}{\numberline {3.2}VCS (Version Control System)}{31}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Introduzione}{31}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Sistema di controllo di versione distribuito: Bitbucket}{32}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Deploy}{34}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}VPS hosting vs Cloud Hosting}{34}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Deploy: Heroku}{35}{subsection.3.3.2}
\contentsline {chapter}{\numberline {4}Conclusioni e sviluppi futuri}{37}{chapter.4}
\contentsline {chapter}{\numberline {5}Riferimenti}{39}{chapter.5}
